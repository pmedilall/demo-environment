# demo-environment

Ansible Even Driven Automation Dynatrace Demo

## Demo/Lab Developers:

*Rafael Minguillon*, EMEA Ansible Tecnical Account Manager, Red Hat

*Carlos Lopez Bartolome*, Specialist Solution Architect, Red Hat

*Cesar Fernandez*, EMEA Ansible Specialist Solution Architect, Red Hat

## Prerequisites

- ServiceNow instance.
- Dynatrace instance.
- OpenShift Cluster +4.12 with admin rights.
- Ansible Navigator.
- Demo Environment project.

### ServiceNow

You can request a ServiceNow Developer Instance in the following [link](https://developer.servicenow.com/).

### Dynatrace

You can request a Dynatrace Trial Instance in the following [link](https://www.dynatrace.com/signup/).

### OpenShift

If you don't have an OpenShift Cluster 4.12 with admin rights, you can request one in the following [link](https://demo.redhat.com/catalog?category=Workshops&item=babylon-catalog-prod%2Fsandboxes-gpte.ocp412-wksp.prod).

### Ansible Navigator

- If you have Linux, you can install the `ansible-navigator` tool following the steps described in the following [link](https://ansible-navigator.readthedocs.io/installation/#linux).

> Note: Run the command `ansible-navigator --version` to make sure it was installed correctly.

## How to deploy the demo environment

### Demo Environment Project

- Clone the demo environment project:

```sh
cd ~
git clone https://gitlab.com/ansible-eda-dynatrace-demo/demo-environment.git
```

### Deploy Demo Environment

- Create the demo configuration variables file, replacing the highlighted variables:

```sh
cat << EOF > demo-environment/vars/demo-config.yml
---
# Demo vars
demo_namespace: ansible-eda-demo
demo_templates_path: templates

# ServiceNow vars
snow_instance_host: "*<SNOW_HOST>*"
snow_instance_username: "*<SNOW_USERNAME>*"
snow_instance_password: "*<SNOW_PASSWORD>*"
snow_eda_username: "ansible-eda"
snow_eda_password: "RedHat!123"

# OCP vars
ocp_templates_path: "{{ demo_templates_path }}/images"

# Gitea vars
gitea_templates_path: "{{ demo_templates_path }}/gitea"

# OpenShift GitOps vars
gitops_operator_templates_path: "{{ demo_templates_path }}/gitops/operator"
gitops_config_templates_path: "{{ demo_templates_path }}/gitops/config"

# Dynatrace vars
dynatrace_operator_templates_path: "{{ demo_templates_path }}/dynatrace/operator"
dynatrace_instance_templates_path: "{{ demo_templates_path }}/dynatrace/instance"
dynatrace_instance_host: "*<DYNATRACE_HOST>*"
dynatrace_api_token: "*<DYNATRACE_API_TOKEN>*"
dynatrace_data_token: "*<DYNATRACE_DATA_TOKEN>*"

# Ansible vars
ansible_operator_templates_path: "{{ demo_templates_path }}/ansible/operator"
ansible_eda_instance_templates_path: "{{ demo_templates_path }}/ansible/instance"
ansible_eda_instance_admin_password: redhat
ansible_instance_templates_path: "{{ demo_templates_path }}/ansible/instance"
ansible_instance_admin_password: redhat
ansible_instance_manifest: "*<AAP_MANIFEST>*" --> !!!ENCODE IN BASE64!!! (base64 /path/to/ansible_manifest.zip)
```

> Note: You can generate an Ansible Automation Platform manifest following the steps described in the following [link](https://docs.ansible.com/ansible-tower/latest/html/userguide/import_license.html#obtain-sub-manifest).

- Deploy the demo environment:

```sh
cd ~/demo-environment/ansible-navigator
ansible-navigator run ../ocp-demo-deploy.yml -m stdout \
  -e 'ansible_python_interpreter=/usr/bin/python3' \
  -e 'openshift_api=<OPENSHIFT_URL>' \
  -e 'openshift_token=<OPENSHIFT_API>' \
  -e 'openshift_storage_class=gp3-csi'
```

## Configure Dynatrace
1. On the console window go to `Settings/Log Monitoring/Log storage configuration`. Add a new rule:
   - Rule name: `watches-eshop`
   - Rule type: `Include in storage`
   - Matcher: 
       - Matcher attribute: `K8s namespace name`
       - Matcher value: `watches-eshop`

2. Go to `Settings/Log Monitoring/Events extraction`. Add a new log event:
   - Summary: `watches-eShop Error`
   - Log query: `matchesValue(event.type, "log") and matchesPhrase(content, "Memory Error") and matchesValue(k8s.namespace.name, "watches-eshop")`
   - Event type: `Error`
   - Event template:
       - Title: `Watches-eShop Error`
       - Description: 
```
Log Source Error -> {log.source}
In Namespace: {k8s.namespace.name}
```

3. Go to `Access Tokens` App. Generate new Token:
   - Token name: `Ansible EDA`
   - Select scopes from the table below:
       - Read Problems
       - Write Problems
       - Access problem and event feed, metrics, and topology
       - Read configuration
       - Write configuration